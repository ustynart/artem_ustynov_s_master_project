# Artem_Ustynov_s_Master_Project

### Folders content:

```
executables folder - sandbox app and InnoSetup installer to test it
src folder:
   code - source code of the sandbox app and cmake file	

   thesis - source code of the thesis

text folder - thesis in pdf format
```
root folder executables.rar, src.rar archives that can be used to check if data was damaged

### Compilation :
To compile 32 bit version set bit variable to 32 and use 32bit toolchain in combination with provided CMkake
To compile 64 bit version set variable to 64 and use 64 bit toolchain
Tests will attempt reading data from C disk, so in their manifest they should ask for administrative privileges.
test2 expected to be 32bit and test - 64bit executable.

### Other:
To run test
1. run Windows shell as admin
2. navigate to "./executables" folder
3. run test.bat

To try out run InnoSetup in sandboxed mode
1. run Windows shell as admin
2. navigate to "./executables" folder
3. run "./Sandbox.exe ./innosetup-6.2.0.exe"
4. sql file and my_swamp folder will be created. Sql file will contain accessed registers 
and my_swamp will contain all the files that were created or opened with "write" permisson 