// test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <map>
#include <string.h>
#include <windows.h>
#include <winternl.h>
#include <ostream>
#include <stdio.h>
#include <strsafe.h>
#include <string.h>
#include <cassert>

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    LPTSTR lpEnd;
    LONG lResult;
    DWORD dwSize;
    TCHAR szName[MAX_PATH];
    HKEY hKey, result;
    FILETIME ftWrite;
    DWORD disposition;

    //    ====TESTING REGISTERS====
    auto status = RegCreateKeyW(HKEY_CURRENT_USER, LR"(app32\test)", &result);
    if (status != ERROR_SUCCESS) {
        exit(1);
    }

    auto b = L"test";
    status = RegSetValueExW(result, L"value1", NULL, REG_SZ, (const BYTE*)b, 10);
    if (status != ERROR_SUCCESS) {
        exit(2);
    }

    wchar_t c[100];
    DWORD type;
    DWORD size = 10;
    status = RegQueryValueExW(result, L"value1", NULL, &type, (BYTE*)&c, &size);
    if (status != ERROR_SUCCESS) {
        exit(3);
    }
    assert(wcscmp(L"test", c) == 0);

    status = RegCloseKey(result);
    if (status != ERROR_SUCCESS) {
        exit(4);
    }

    status = RegOpenKeyW(HKEY_CURRENT_USER, LR"(app32\test)", &result);
    if (status != ERROR_SUCCESS) {
        exit(5);
    }

    auto d = L"test2";
    status = RegSetValueExW(result, L"value1", NULL, REG_SZ, (const BYTE*)d, 12);
    if (status != ERROR_SUCCESS) {
        exit(6);
    }

    size = 12;
    status = RegQueryValueExW(result, L"value1", NULL, &type, (BYTE*)&c, &size);
    printf("%ld", status);
    if (status != ERROR_SUCCESS) {
        exit(11);
    }

    assert(wcscmp(L"test2", c) == 0);

    status = RegCloseKey(result);
    if (status != ERROR_SUCCESS) {
        exit(7);
    }

    /** ====TESTING FILES==== **/

    FILE* fp;

    fopen_s(&fp, "C:\\test32.txt", "w+");
    if (!fp) {
        fclose(fp);
        exit(8);
    }
    fprintf(fp, "This is testing for fprintf...");
    fclose(fp);

    fopen_s(&fp, "C:\\test232.txt", "w+");
    if (!fp) {
        fclose(fp);
        exit(9);
    }
    fprintf(fp, "overriding sensetive data");
    fclose(fp);

    FILE* file;
    fopen_s(&file,"C:\\test32.txt", "r");

    if (file == NULL) {
        exit(10);
    }

    fseek(file, 0, SEEK_END);
    long length = ftell(file);
    fseek(file, 0, SEEK_SET);
    char* buffer = (char *)malloc(sizeof(char) * (length + 1));
    buffer[length] = '\0';
    fread(buffer, sizeof(char), length, file);
    fclose(file);

    if (strncmp("This is testing for fprintf...", buffer,length) != 0) {
        exit(12);
    }
    free(buffer);

    FILE* file2;
    fopen_s(&file2, "C:\\test232.txt", "r");

    if (file2 == NULL) {
        exit(10);
    }
    fseek(file2, 0, SEEK_END);
    length = ftell(file2);
    fseek(file2, 0, SEEK_SET);
    char* buffer2 = (char*)malloc(sizeof(char) * (length + 1));
    buffer2[length] = '\0';
    fread(buffer2, sizeof(char), length, file2);
    fclose(file2);


    if (strncmp("overriding sensetive data", buffer2, length) != 0) {
        exit(13);
    }
    free(buffer2);


    return 0;
}

