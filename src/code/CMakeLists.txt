cmake_minimum_required(VERSION 3.20)
project(MasterProject)

set(CMAKE_CXX_STANDARD 17)
set(BIT 32)
include_directories(${CMAKE_SOURCE_DIR}/src/sql)

add_library(SandboxDll${BIT} SHARED src/dll/SandboxDll.cpp src/dll/headers/SandboxDll.h src/dll/Hijacker.cpp src/dll/headers/Hijacker.h
        src/dll/headers/OriginalWindowsDefinitions.h src/dll/OriginalWindowsDefinitions.cpp
        src/dll/RegisterManager.cpp src/dll/headers/RegisterManager.h src/dll/headers/Defines.h
        src/dll/headers/FileManager.h src/dll/FileManager.cpp src/dll/headers/FileManager.h src/dll/FileManager.cpp src/dll/headers/FileManager.h)

add_executable(Injector${BIT} src/injector/Injector.cpp src/injector/headers/Injector.h src/injector/main.cpp )

add_executable(test src/test/tests32.cpp)
add_executable(test2 src/test/tests64.cpp)

add_executable(Sandbox src/app/SandboxApp.cpp src/app/headers/Initializer.h src/app/Initializer.cpp)
message("CMAKE_GENERATOR_TOOLSET: ${CMAKE_GENERATOR_TOOLSET}")

find_library(SQLITE NAMES sqlite3)
target_link_libraries(SandboxDll${BIT} LINK_PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/sql/sqlite3-${BIT}.lib)