//
// Created by Artem Ustynov on 22.03.2022.
//

#include "headers/Hijacker.h"

RegisterManager Hijacker::registerManager;
FileManager Hijacker::fileManager;

wstring Hijacker::injectorPath;

CRITICAL_SECTION Hijacker::criticalSectionMeta;
CRITICAL_SECTION Hijacker::criticalSectionProcess;
CRITICAL_SECTION Hijacker::criticalSectionCore;
CRITICAL_SECTION Hijacker::criticalSectionLoadLibrary;

char *Hijacker::NONAME = NULL;
bool Hijacker::ORIGINAL_IAT = true;
bool Hijacker::INITIALIZED = false;

void Hijacker::initCriticalSections() {
    if (INITIALIZED) return;
    if (!InitializeCriticalSectionAndSpinCount(&criticalSectionMeta, 0x00000400))
        exit(3);
    if (!InitializeCriticalSectionAndSpinCount(&criticalSectionProcess, 0x00000400))
        exit(3);
    if (!InitializeCriticalSectionAndSpinCount(&criticalSectionCore, 0x00000400))
        exit(3);
    if (!InitializeCriticalSectionAndSpinCount(&criticalSectionCore, 0x00000400))
        exit(3);
    if (!InitializeCriticalSectionAndSpinCount(&criticalSectionLoadLibrary, 0x00000400))
        exit(3);
    INITIALIZED = true;
}

void Hijacker::checkRVA(MAX_WORD RVA, int id) {
    if (RVA == 0) {
        throw runtime_error("RVA error " + to_string(id));
    }
}

NTSTATUS Hijacker::myNtCreateKey(PHANDLE KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                                 ULONG TitleIndex, PUNICODE_STRING Class, ULONG CreateOptions, PULONG Disposition) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {

        auto res = OriginalWindowsDefinitions::originalNtCreateKey
                (KeyHandle, DesiredAccess, ObjectAttributes, TitleIndex, Class, CreateOptions, Disposition);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        registerManager.ntCreateKey(KeyHandle, DesiredAccess, ObjectAttributes, TitleIndex, Class, CreateOptions,
                                    Disposition);
        LeaveCriticalSection(&criticalSectionCore);
        return ERROR_SUCCESS;
    } catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

NTSTATUS Hijacker::myNtOpenKey(PHANDLE KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                               ULONG OpenOptions) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions
        ::originalNtOpenKeyEx(KeyHandle, DesiredAccess, ObjectAttributes, OpenOptions);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = registerManager.ntOpenKey(KeyHandle, DesiredAccess, ObjectAttributes, OpenOptions);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

NTSTATUS Hijacker::myNtSetValueKey(HANDLE KeyHandle, PUNICODE_STRING ValueName, ULONG TitleIndex, ULONG Type,
                                   PVOID Data, ULONG DataSize) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions
        ::originalNtSetValueKey(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = registerManager.ntSetValue(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

NTSTATUS Hijacker::myNtQueryValueKey(HANDLE KeyHandle, PUNICODE_STRING ValueName,
                                     KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass, PVOID KeyValueInformation,
                                     ULONG Length, PULONG ResultLength) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions
        ::originalNtQueryValueKey(KeyHandle, ValueName, KeyValueInformationClass, KeyValueInformation, Length,
                                  ResultLength);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = registerManager.ntQueryValueKey(KeyHandle, ValueName, KeyValueInformationClass,
                                                   KeyValueInformation, Length, ResultLength);

        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

NTSTATUS
Hijacker::myNtQueryKey(HANDLE KeyHandle, KEY_INFORMATION_CLASS KeyInformationClass, PVOID KeyInformation, ULONG Length,
                       PULONG ResultLength) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions
        ::originalNtQueryKey(KeyHandle, KeyInformationClass, KeyInformation, Length, ResultLength);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = registerManager.ntQueryKey(KeyHandle, KeyInformationClass, KeyInformation, Length, ResultLength);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }

}

NTSTATUS Hijacker::myNtRegCloseKey(HANDLE handle) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions::originalNtCloseKey(handle);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = registerManager.ntCloseKey(handle);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

NTSTATUS Hijacker::myNtOpenFile(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                                PIO_STATUS_BLOCK IoStatusBlock, ULONG ShareAccess, ULONG OpenOptions) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions
        ::originalNtOpenFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, ShareAccess, OpenOptions);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = fileManager.ntOpenFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock,
                                          ShareAccess, OpenOptions);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(2);
    }
}

NTSTATUS Hijacker::myNtCreateFile(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                                  PIO_STATUS_BLOCK IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes,
                                  ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer,
                                  ULONG EaLength) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions::originalNtCreateFile
                (FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize,
                 FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = fileManager.ntCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock,
                                            AllocationSize, FileAttributes, ShareAccess, CreateDisposition,
                                            CreateOptions, EaBuffer, EaLength);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

NTSTATUS
Hijacker::myNtQueryAttributesFile(POBJECT_ATTRIBUTES ObjectAttributes, PFILE_BASIC_INFORMATION FileInformation) {
    EnterCriticalSection(&criticalSectionCore);

    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions::originalNtQueryAttributesFile(ObjectAttributes, FileInformation);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = fileManager.ntQueryAttributesFile(ObjectAttributes, FileInformation);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

BOOL Hijacker::myMoveFileW(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions::originalMoveFileW(lpExistingFileName, lpNewFileName);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = fileManager.moveFileW(lpExistingFileName, lpNewFileName);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    } catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

BOOL Hijacker::myMoveFileExW(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName, DWORD dwFlags) {
    EnterCriticalSection(&criticalSectionCore);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions::originalMoveFileExw(lpExistingFileName, lpNewFileName, dwFlags);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    try {
        auto res = fileManager.moveFileExW(lpExistingFileName, lpNewFileName, dwFlags);
        LeaveCriticalSection(&criticalSectionCore);
        return res;
    }
    catch (exception &e) {
        LeaveCriticalSection(&criticalSectionCore);
        exit(1);
    }
}

BOOL
Hijacker::myCreateProcessW(LPCWSTR lpApplicationName, LPWSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes,
                           LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags,
                           LPVOID lpEnvironment, LPCWSTR lpCurrentDirectory, LPSTARTUPINFOW lpStartupInfo,
                           LPPROCESS_INFORMATION lpProcessInformation) {
    EnterCriticalSection(&criticalSectionProcess);

    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions::originalCreateProcessW
                (lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes, bInheritHandles,
                 CREATE_SUSPENDED, lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);
        LeaveCriticalSection(&criticalSectionProcess);
        return res;
    }
// CREATE_SUSPENDED
    try {
        auto res = fileManager.createProcessW(lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes,
                                              bInheritHandles, CREATE_SUSPENDED, lpEnvironment, lpCurrentDirectory,
                                              lpStartupInfo, lpProcessInformation);
        loadSandboxByPID(lpProcessInformation->dwProcessId);

        ResumeThread(lpProcessInformation->hThread);
        LeaveCriticalSection(&criticalSectionProcess);
        return res;
    } catch (exception &e) {
        LeaveCriticalSection(&criticalSectionProcess);
        exit(1);
    }
}

HMODULE Hijacker::myLoadLibraryW(LPCWSTR lpLibFileName) {
    EnterCriticalSection(&criticalSectionLoadLibrary);
    if (RegisterManager::SQL_OPERATION) {
        auto res = OriginalWindowsDefinitions::originalLoadLibraryW(lpLibFileName);
        LeaveCriticalSection(&criticalSectionLoadLibrary);
        return res;
    }
    try {
        FileManager::libOriginalCall = true;
        auto res = fileManager.loadLibraryW(lpLibFileName);
        locateAndSwapAll((PIMAGE_DOS_HEADER) res, STEAL);
        FileManager::libOriginalCall = false;
        LeaveCriticalSection(&criticalSectionLoadLibrary);
        return res;
    } catch (exception &e) {
        LeaveCriticalSection(&criticalSectionLoadLibrary);
        exit(1);
    }
}

void Hijacker::locateAndSwapAll(PIMAGE_DOS_HEADER pimageDosHeader, Hijacker::ACTION action) {
    auto ptrMyNtCreateKey = myNtCreateKey;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)
            (PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG, PUNICODE_STRING, ULONG, PULONG)>
            (pimageDosHeader, "ntdll.dll", "NtCreateKey", ptrMyNtCreateKey,
             OriginalWindowsDefinitions::originalNtCreateKey, action);

    auto ptrNtOpenKey = myNtOpenKey;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG)>
            (pimageDosHeader,
             "ntdll.dll", "NtOpenKeyEx", ptrNtOpenKey, OriginalWindowsDefinitions::originalNtOpenKeyEx, action);

    auto ptrNtSetValueKey = myNtSetValueKey;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)(HANDLE, PUNICODE_STRING, ULONG, ULONG, PVOID, ULONG)>
            (pimageDosHeader,
             "ntdll.dll", "NtSetValueKey", ptrNtSetValueKey, OriginalWindowsDefinitions::originalNtSetValueKey,
             action);

    auto ptrNtQueryValueKeyExW = myNtQueryValueKey;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)
            (HANDLE, PUNICODE_STRING, KEY_VALUE_INFORMATION_CLASS, PVOID, ULONG, PULONG)>
            (pimageDosHeader,
             "ntdll.dll", "NtQueryValueKey", ptrNtQueryValueKeyExW,
             OriginalWindowsDefinitions::originalNtQueryValueKey, action);

    auto ptrNtQueryKeyExW = myNtQueryKey;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)(HANDLE, KEY_INFORMATION_CLASS, PVOID, ULONG, PULONG)>
            (pimageDosHeader,
             "ntdll.dll", "NtQueryKey", ptrNtQueryKeyExW, OriginalWindowsDefinitions::originalNtQueryKey, action);

    auto ptrNtRegCloseKey = myNtRegCloseKey;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)(HANDLE)>
            (pimageDosHeader, "ntdll.dll", "NtClose", ptrNtRegCloseKey,
             OriginalWindowsDefinitions::originalNtCloseKey, action);

    auto ptrNtOpenFile = myNtOpenFile;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)
            (PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK, ULONG, ULONG)>
            (pimageDosHeader, "ntdll.dll", "NtOpenFile", ptrNtOpenFile,
             OriginalWindowsDefinitions::originalNtOpenFile, action);

    auto ptrNtCreateFile = myNtCreateFile;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)
            (PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK, PLARGE_INTEGER,
             ULONG, ULONG, ULONG, ULONG, PVOID, ULONG)>
            (pimageDosHeader, "ntdll.dll", "NtCreateFile", ptrNtCreateFile,
             OriginalWindowsDefinitions::originalNtCreateFile, action);

    auto ptrNtQueryAttributesFile = myNtQueryAttributesFile;
    locateFunctionAndSwap<NTSTATUS(NTAPI *)(POBJECT_ATTRIBUTES, PFILE_BASIC_INFORMATION)>
            (pimageDosHeader, "ntdll.dll", "NtQueryAttributesFile", ptrNtQueryAttributesFile,
             OriginalWindowsDefinitions::originalNtQueryAttributesFile, action);

    auto ptrMyMoveFileW = myMoveFileW;
    locateFunctionAndSwap<BOOL(WINAPI *)(LPCWSTR, LPCWSTR)>
            (pimageDosHeader,
             "Kernel32.dll", "MoveFileW", ptrMyMoveFileW, OriginalWindowsDefinitions::originalMoveFileW, action);

    auto ptrMyMoveFileExW = myMoveFileExW;
    locateFunctionAndSwap<BOOL(WINAPI *)(LPCWSTR, LPCWSTR, DWORD)>
            (pimageDosHeader,
             "Kernel32.dll", "MoveFileExW", ptrMyMoveFileExW, OriginalWindowsDefinitions::originalMoveFileExw,
             action);

    //**NON NTDLL functions**//
    auto ptrMyCreateProcessW = myCreateProcessW;
    locateFunctionAndSwap<BOOL(WINAPI *)(LPCWSTR, LPWSTR, LPSECURITY_ATTRIBUTES, LPSECURITY_ATTRIBUTES, BOOL,
                                         DWORD, LPVOID, LPCWSTR, LPSTARTUPINFOW, LPPROCESS_INFORMATION)>
            (pimageDosHeader,
             "Kernel32.dll", "CreateProcessW", ptrMyCreateProcessW,
             OriginalWindowsDefinitions::originalCreateProcessW, action);

    auto ptrMyLoadLibraryW = myLoadLibraryW;
    locateFunctionAndSwap<HMODULE(WINAPI *)(LPCWSTR)>
            (pimageDosHeader, "Kernel32.dll", "LoadLibraryW", ptrMyLoadLibraryW,
             OriginalWindowsDefinitions::originalLoadLibraryW, action);
}

template<typename T>
void Hijacker::functionSwap(char *&fName, PIMAGE_IMPORT_DESCRIPTOR &pImageImpDescArray, PIMAGE_DOS_HEADER &pDosHeader,
                            PIMAGE_NT_HEADERS &pNTHeaders, MAX_WORD func1, size_t &imageDescIndex, size_t &thunkIndex,
                            T &func2) {
    size_t vp_res = 0;
    DWORD prev_vp_val = 9;
    DWORD page_original_value = 0x02;

    checkRVA(pImageImpDescArray[imageDescIndex].FirstThunk, 0);
    auto pThunkArr = (PIMAGE_THUNK_DATA) (pImageImpDescArray[imageDescIndex].FirstThunk + ((BYTE *) pDosHeader));
    vp_res = VirtualProtect(pThunkArr + thunkIndex, sizeof(pThunkArr[thunkIndex]), PAGE_READWRITE,
                            &page_original_value);
    if (vp_res == 0) {
        throw exception("critical init error Virtual Protect fail. shutting down immediately!");
    }
    MAX_WORD original_function_address;
    if ((pThunkArr[thunkIndex].u1.Ordinal & IMAGE_ORDINAL_FLAG) != 0) {
        auto pImageExpDescArray = (PIMAGE_EXPORT_DIRECTORY)
                (pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress +
                 ((BYTE *) pDosHeader));
        auto ordinal = (DWORD32) pThunkArr[thunkIndex].u1.Ordinal & 0x0000ffff;
        checkRVA(pImageExpDescArray[0].AddressOfFunctions, 1);
        auto funk_arr = (PDWORD32) (pImageExpDescArray[0].AddressOfFunctions + ((BYTE *) pDosHeader));
        original_function_address = (MAX_WORD) (
                funk_arr[ordinal - pImageExpDescArray[0].Base] + ((BYTE *) pDosHeader));
        funk_arr[ordinal - pImageExpDescArray[0].Base] = func1 - (MAX_WORD) pDosHeader;
    } else {
        original_function_address = pThunkArr[thunkIndex].u1.Function;
        pThunkArr[thunkIndex].u1.Function = func1;
    }

    if ((MAX_WORD) original_function_address != (MAX_WORD) func1)
        func2 = (T) original_function_address;

    vp_res = VirtualProtect(pThunkArr + thunkIndex, sizeof(pThunkArr[thunkIndex]), page_original_value,
                            &prev_vp_val);
    if (vp_res == 0) {
        throw exception("critical init error Virtual Protect fail. shutting down immediately!");
    }
}

char *WINAPI
Hijacker::getFunctionName(IMAGE_THUNK_DATA &imageThunkData, PIMAGE_DOS_HEADER &pDosHeader,
                          PIMAGE_NT_HEADERS &pNTHeaders) {
    checkRVA(imageThunkData.u1.AddressOfData, 2);
    auto funk = (PIMAGE_IMPORT_BY_NAME) (imageThunkData.u1.AddressOfData + ((BYTE *) pDosHeader));
    if ((imageThunkData.u1.Ordinal & IMAGE_ORDINAL_FLAG) != 0) {

        checkRVA(pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress, 3);
        auto pImageExpDescArray = (PIMAGE_EXPORT_DIRECTORY)
                (pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress +
                 ((BYTE *) pDosHeader));
        auto ordinal = (WORD) imageThunkData.u1.Ordinal;
        if (pImageExpDescArray[0].AddressOfNames == NULL)
            return nullptr;
        checkRVA(pImageExpDescArray[0].AddressOfNameOrdinals, 4);
        auto name_ordinal_array = (PWORD) (pImageExpDescArray[0].AddressOfNameOrdinals + ((BYTE *) pDosHeader));

        int index = 0;

        for (; index <= pImageExpDescArray[0].NumberOfNames; ++index) {
            if (name_ordinal_array[index] == ordinal)
                break;
            if (pImageExpDescArray[0].NumberOfNames == index)
                return NONAME;
        }
        auto funk_name_arr = (PDWORD32) (pImageExpDescArray[0].AddressOfNames + ((BYTE *) pDosHeader));
        return (char *) (funk_name_arr[index] + ((BYTE *) pDosHeader));

    } else {
        return funk->Name;
    }
}

template<typename T>
void Hijacker::locateFunctionAndSwap(PIMAGE_DOS_HEADER pDosHeader, const char *dllName, const char *replaceFunkName,
                                     T &func1, T &func2, Hijacker::ACTION action) {
    T &f1 = func1;
    T &f2 = func2;
    if (action != STEAL) {
        T &tmp = f1;
        f1 = func2;
        f2 = tmp;
    }
    checkRVA(pDosHeader->e_lfanew, 5);
    auto pNTHeaders = (PIMAGE_NT_HEADERS) (((BYTE *) pDosHeader) + pDosHeader->e_lfanew);
    auto pImageImpDescArray = (PIMAGE_IMPORT_DESCRIPTOR)
            (pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress +
             ((BYTE *) pDosHeader));
    size_t size = pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size /
                  sizeof(IMAGE_IMPORT_DESCRIPTOR);

    for (size_t imageDescIndex = 0; imageDescIndex < size; imageDescIndex++) {
        if (pImageImpDescArray[imageDescIndex].Characteristics == NULL) break;
        checkRVA(pImageImpDescArray[imageDescIndex].Name, 7);

        if (_stricmp(dllName, (char *) (pImageImpDescArray[imageDescIndex].Name + ((BYTE *) pDosHeader))) != 0)
            continue;
        checkRVA(pImageImpDescArray[imageDescIndex].OriginalFirstThunk, 8);
        auto pOriginalThunkArr = (PIMAGE_THUNK_DATA) (pImageImpDescArray[imageDescIndex].OriginalFirstThunk +
                                                      ((BYTE *) pDosHeader));
        for (size_t thunkIndex = 0; true; thunkIndex++) {
            if (pOriginalThunkArr[thunkIndex].u1.AddressOfData == 0) break;
            char *f_name = getFunctionName(pOriginalThunkArr[thunkIndex], pDosHeader, pNTHeaders);
            if (strcmp(replaceFunkName, f_name) == 0) {
                functionSwap<T>(f_name, pImageImpDescArray, pDosHeader, pNTHeaders, (MAX_WORD) f1, imageDescIndex,
                                thunkIndex, f2);
                return;
            }
        }
    }
}

void Hijacker::loadSandboxByPID(size_t pid) {
    HANDLE hProcess = OpenProcess(PROCESS_VM_WRITE | PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION, FALSE, pid);
    if (!hProcess) {
        throw exception("Failed to open the debugged app.\n");
    }
    BOOL bit_32;
    if (! IsWow64Process(hProcess, &bit_32))
        exit(7);
    PROCESS_INFORMATION pi{};
    STARTUPINFOW si{}; // initialize with zeroes.
    si.cb = sizeof(STARTUPINFOW);
    auto args = wstring(L" ") + to_wstring(pid);
    if (bit_32) {
        wstring name = injectorPath + LR"(\Injector32.exe)";
        if (OriginalWindowsDefinitions
        ::originalCreateProcessW(name.data(), args.data(), nullptr, nullptr, TRUE, 0, nullptr, nullptr, &si, &pi)) {
            DWORD exit_code = 0;
            WaitForSingleObject(pi.hProcess, INFINITE);
            GetExitCodeProcess(pi.hProcess, &exit_code);
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            if (exit_code!=ERROR_SUCCESS){
                exit(exit_code);
            }
        } else {
            exit(1);
        }
    } else {
        wstring name = injectorPath + LR"(\Injector64.exe)";
        if (OriginalWindowsDefinitions::originalCreateProcessW(name.data(), args.data(), nullptr, nullptr, TRUE, 0, nullptr,
                                                               nullptr, &si, &pi)) {
            DWORD exit_code = 0;
            WaitForSingleObject(pi.hProcess, INFINITE);
            GetExitCodeProcess(pi.hProcess, &exit_code);
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            if (exit_code!=ERROR_SUCCESS){
                exit(exit_code);
            }
        } else {
            exit(1);
        }
    }
    CloseHandle(hProcess);
}

void Hijacker::init() {
    EnterCriticalSection(&criticalSectionMeta);
    if (!ORIGINAL_IAT) {
        LeaveCriticalSection(&criticalSectionMeta);
        return;
    }

    try {
        FileManager::swampPath = filesystem::current_path().wstring();
        injectorPath = filesystem::current_path().wstring();
        PTEB teb = (PTEB) NtCurrentTeb();
        PPEB peb = teb->ProcessEnvironmentBlock;
        PPEB_LDR_DATA peb_ldr_data = peb->Ldr;
        PLIST_ENTRY list_head = &peb_ldr_data->InMemoryOrderModuleList;
        PLIST_ENTRY list_entry = list_head;
        while ((list_entry = list_entry->Flink) != list_head) {
            PLDR_DATA_TABLE_ENTRY data_table_entry =
                    CONTAINING_RECORD(list_entry, LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks);
            locateAndSwapAll((PIMAGE_DOS_HEADER) data_table_entry->DllBase, STEAL);
        }
        ORIGINAL_IAT = false;
        LeaveCriticalSection(&criticalSectionMeta);
    } catch (exception &e) {
        LeaveCriticalSection(&criticalSectionMeta);
        exit(2);
    }
}

void Hijacker::done() {
    EnterCriticalSection(&criticalSectionMeta);
    if (ORIGINAL_IAT) {
        LeaveCriticalSection(&criticalSectionMeta);
        return;
    }

    try {
        PTEB teb = (PTEB) NtCurrentTeb();
        PPEB peb = teb->ProcessEnvironmentBlock;
        PPEB_LDR_DATA peb_ldr_data = peb->Ldr;
        PLIST_ENTRY list_head = &peb_ldr_data->InMemoryOrderModuleList;
        PLIST_ENTRY list_entry = list_head;

        while ((list_entry = list_entry->Flink) != list_head) {
            PLDR_DATA_TABLE_ENTRY data_table_entry = CONTAINING_RECORD(list_entry, LDR_DATA_TABLE_ENTRY,
                                                                       InMemoryOrderLinks);
            locateAndSwapAll((PIMAGE_DOS_HEADER) data_table_entry->DllBase, UNSTEAL);
        }
        ORIGINAL_IAT = true;
        LeaveCriticalSection(&criticalSectionMeta);
    } catch (exception &e) {
        LeaveCriticalSection(&criticalSectionMeta);
        exit(2);
    }
}