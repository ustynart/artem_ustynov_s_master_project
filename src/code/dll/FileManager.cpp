#include "headers/FileManager.h"

/**
 * relative is path NOT supported
 * Multithreading is handled in hijacker.cpp"
 * **/

bool FileManager::libOriginalCall = false;
bool FileManager::SQL_OPERATION = false;
bool FileManager::originalCall = false;
wstring FileManager::swampPath;
vector<wstring *> FileManager::allocatedWstrings;
vector<UNICODE_STRING *> FileManager::allocatedUstrings;


wstring *FileManager::generateNewPath(wstring &originalPath) {
    wstring orig_name_reg = regex_replace(originalPath, specialChars, LR"(_)");
    auto index = originalPath.find_last_of(L'\\');
    if (index == string::npos) {
        exit(8);
    }
    //relative path not supported
    if (originalPath.length() < 3) {
        exit(9);
    }

    const auto file_name = originalPath.substr(index + 1, originalPath.length() - index - 1);
    auto new_path = new(nothrow)
            wstring(LR"(\??\)" + swampPath + wstring(LR"(\my_swamp\)") + orig_name_reg + L'_' + file_name);
    if (!new_path) {
        exit(4);
    }
    allocatedWstrings.push_back(new_path);
    return new_path;
}

PUNICODE_STRING FileManager::generateNewUnicode(wstring *newPath) {
    auto *s = new(nothrow) UNICODE_STRING;

    if (!s) {
        exit(4);
    }
    allocatedUstrings.push_back(s);

    s->Buffer = newPath->data();
    s->Length = (newPath->length()) * sizeof(WCHAR);
    s->MaximumLength = (newPath->length() + 1) * sizeof(WCHAR);
    return s;
}

NTSTATUS FileManager::ntOpenFile(PHANDLE &FileHandle, ACCESS_MASK &DesiredAccess, POBJECT_ATTRIBUTES &ObjectAttributes,
                                 PIO_STATUS_BLOCK &IoStatusBlock, ULONG &ShareAccess, ULONG &OpenOptions) {

    bool write = (DesiredAccess & (GENERIC_WRITE | WRITE_DAC | WRITE_OWNER | DELETE)) != 0;

    if (originalCall || !write || libOriginalCall) {// || !write || index == wstring::npos
        if (translation_map.find(ObjectAttributes->ObjectName->Buffer) != end(translation_map)) {
            wstring originalPath(ObjectAttributes->ObjectName->Buffer);
            auto new_path = generateNewPath(originalPath);
            ObjectAttributes->ObjectName = generateNewUnicode(new_path);
        }
        return OriginalWindowsDefinitions
        ::originalNtOpenFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, ShareAccess, OpenOptions);
    }

    wstring originalPath(ObjectAttributes->ObjectName->Buffer);
    auto new_path = generateNewPath(originalPath);
    ObjectAttributes->ObjectName = generateNewUnicode(new_path);

    originalCall = true;
    CopyFileW(originalPath.data(), new_path->data(), true);
    originalCall = false;
    SetLastError(ERROR_SUCCESS);

    auto res = OriginalWindowsDefinitions
    ::originalNtOpenFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, ShareAccess, OpenOptions);
    if (res == STATUS_SUCCESS) translation_map[originalPath] = *new_path;

    return res;
}

NTSTATUS
FileManager::ntCreateFile(PHANDLE &FileHandle, ACCESS_MASK &DesiredAccess, POBJECT_ATTRIBUTES &ObjectAttributes,
                          PIO_STATUS_BLOCK &IoStatusBlock, PLARGE_INTEGER &AllocationSize, ULONG &FileAttributes,
                          ULONG &ShareAccess, ULONG &CreateDisposition, ULONG &CreateOptions, PVOID &EaBuffer,
                          ULONG &EaLength) {

    if (originalCall || libOriginalCall)// || index == wstring::npos
        return OriginalWindowsDefinitions::originalNtCreateFile
                (FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize,
                 FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);

    wstring originalPath(ObjectAttributes->ObjectName->Buffer);
    auto new_path = generateNewPath(originalPath);
    ObjectAttributes->ObjectName = generateNewUnicode(new_path);

    originalCall = true;
    CopyFileW(originalPath.data(), new_path->data(), true);
    originalCall = false;
    SetLastError(ERROR_SUCCESS);

    auto res = OriginalWindowsDefinitions
    ::originalNtCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize,
                           FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);

    if (res == STATUS_SUCCESS)
        translation_map.emplace(originalPath, *new_path);
    return res;
}

NTSTATUS
FileManager::ntQueryAttributesFile(POBJECT_ATTRIBUTES &ObjectAttributes, PFILE_BASIC_INFORMATION &FileInformation) {
    wstring originalPath(ObjectAttributes->ObjectName->Buffer);

    wstring orig_name_reg = regex_replace(originalPath, specialChars, LR"(_)");
    auto index = originalPath.find_last_of(L'\\');
    if (index == string::npos) {
        exit(8);
    }
    //relative path not supported
    if (originalPath.length() < 3) {
        exit(9);
    }
    const auto file_name = originalPath.substr(index + 1, originalPath.length() - index - 1);
    if (translation_map.find(originalPath) != end(translation_map)) {
        auto new_path = new(nothrow) wstring(translation_map.at(originalPath));
        if (!new_path) {
            exit(4);
        }
        allocatedWstrings.push_back(new_path);
        ObjectAttributes->ObjectName = generateNewUnicode(new_path);
    }
    originalCall = true;
    auto res = OriginalWindowsDefinitions::originalNtQueryAttributesFile(ObjectAttributes, FileInformation);
    originalCall = false;
    return res;
}

BOOL FileManager::moveFileW(LPCWSTR &lpExistingFileName, LPCWSTR &lpNewFileName) {

    wstring originalPath(lpNewFileName);
    auto new_path = generateNewPath(originalPath);
    auto existing = LR"(\??\)" + wstring(lpExistingFileName);

    if (translation_map.find(existing) != end(translation_map)) {
        translation_map.emplace(originalPath, translation_map.at(existing));
        return true;
    }
    translation_map.emplace(originalPath, *new_path);
    originalCall = true;
    auto res = CopyFileW(originalPath.data(), new_path->data(), false);
    originalCall = false;
    return res;
}

BOOL FileManager::moveFileExW(LPCWSTR &lpExistingFileName, LPCWSTR &lpNewFileName, DWORD Flags) {

    wstring originalPath(lpNewFileName);
    auto new_path = generateNewPath(originalPath);
    auto existing = LR"(\??\)" + wstring(lpExistingFileName);

    if (translation_map.find(existing) != end(translation_map)) {
        translation_map.emplace(originalPath, translation_map.at(existing));
        return true;
    }
    translation_map.emplace(originalPath, *new_path);
    originalCall = true;
    auto res = CopyFileW(originalPath.data(), new_path->data(), false);
    originalCall = false;
    return res;
}

HMODULE FileManager::loadLibraryW(LPCWSTR &lpLibFileName) {

    auto existing = LR"(\??\)" + wstring(lpLibFileName);

    if (translation_map.find(existing) != end(translation_map)) {
        originalCall = true;
        auto res = OriginalWindowsDefinitions::originalLoadLibraryW(translation_map.at(existing).data());
        originalCall = false;
        return res;
    }
    originalCall = true;
    auto res = OriginalWindowsDefinitions::originalLoadLibraryW(lpLibFileName);
    originalCall = false;
    return res;
}

BOOL FileManager::createProcessW(LPCWSTR &lpApplicationName, LPWSTR &lpCommandLine,
                                 LPSECURITY_ATTRIBUTES &lpProcessAttributes, LPSECURITY_ATTRIBUTES &lpThreadAttributes,
                                 BOOL &bInheritHandles, DWORD dwCreationFlags, LPVOID &lpEnvironment,
                                 LPCWSTR &lpCurrentDirectory, LPSTARTUPINFOW &lpStartupInfo,
                                 LPPROCESS_INFORMATION &lpProcessInformation) {

    if (lpApplicationName == nullptr && lpCommandLine == nullptr)
        return OriginalWindowsDefinitions::originalCreateProcessW
                (lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes,
                 bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory, lpStartupInfo,
                 lpProcessInformation);
    wstring command_line;
    if (lpApplicationName != nullptr)
        command_line = wstring(lpApplicationName);
    else
        command_line = wstring(lpCommandLine);
    wstring new_command = command_line;

    vector<pair<wstring, wstring>> sorted_path{translation_map.begin(), translation_map.end()};
    sort(sorted_path.begin(), sorted_path.end(),
         [](const pair<wstring, wstring> &l, const pair<wstring, wstring> &r) {
             return l.first.length() < r.first.length();
         });
    reverse(sorted_path.begin(), sorted_path.end());
    bool found = false;
    for (auto[orig_name, subs_name]: sorted_path) {
        orig_name = regex_replace(orig_name, wregex(LR"(^\\\?\?\\)"), L"", regex_constants::format_first_only);
        subs_name = regex_replace(subs_name, wregex(LR"(^\\\?\?\\)"), L"", regex_constants::format_first_only);
        wstring orig_name_reg = regex_replace(orig_name, specialChars, LR"(\$&)");
        if (command_line.find(orig_name) != wstring::npos) {
            new_command = regex_replace(command_line, wregex(orig_name_reg), subs_name);
            found = true;
            break;
        }
    }
    if (!found)
        return OriginalWindowsDefinitions::originalCreateProcessW
                (lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes,
                 bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory, lpStartupInfo,
                 lpProcessInformation);

    return OriginalWindowsDefinitions
    ::originalCreateProcessW(lpApplicationName, new_command.data(), lpProcessAttributes, lpThreadAttributes,
                             bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory, lpStartupInfo,
                             lpProcessInformation);
}

void FileManager::freeMemory() {
    for (auto &pointer: allocatedWstrings) {
        delete pointer;
    }
    allocatedWstrings.clear();
    for (auto &pointer: allocatedUstrings) {
        delete pointer;
    }
    allocatedUstrings.clear();
}

FileManager::~FileManager() {
    freeMemory();
}
