//
// Created by Artem Ustynov on 22.03.2022.
//

#pragma once
/**
 * Stores custom definitions
 */

/**
 * to support 32 and 64 bit pointers this define exists
 */
#ifdef _WIN64
#define MAX_WORD DWORD64
#else
#define MAX_WORD DWORD32
#endif