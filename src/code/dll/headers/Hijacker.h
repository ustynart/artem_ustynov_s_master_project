//
// Created by Artem Ustynov on 22.03.2022.
//

#pragma once

#include <windows.h>
#include <winternl.h>
#include "RegisterManager.h"
#include "FileManager.h"
#include <filesystem>

class Hijacker {
private:
    static RegisterManager registerManager;
    static FileManager fileManager;

    /**
     * if IAT is original it should be replaced
     */
    static bool ORIGINAL_IAT;
    static char *NONAME;
    static bool INITIALIZED;
    /**
     * action of hijackings or returning original functions
     */
    enum ACTION {
        STEAL, UNSTEAL
    };

    static CRITICAL_SECTION criticalSectionMeta;
    static CRITICAL_SECTION criticalSectionProcess;
    static CRITICAL_SECTION criticalSectionCore;
    static CRITICAL_SECTION criticalSectionLoadLibrary;

    static wstring injectorPath;

public:
    /**
     * initialize critical sections
     */
    static void initCriticalSections();

    /**
     * check if provided RVA value is null. if it is fail the process
     * @param RVA value
     * @param id id for debug purposes
     */
    static void WINAPI checkRVA(MAX_WORD RVA, int id);

    /**
     * function that will replace original one. for parameters and more documentation check MSDN
     */
    static NTSTATUS
    NTAPI myNtCreateKey(PHANDLE KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                        ULONG TitleIndex, PUNICODE_STRING Class, ULONG CreateOptions, PULONG Disposition);

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static NTSTATUS NTAPI myNtOpenKey(PHANDLE KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                                      ULONG OpenOptions);

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static NTSTATUS NTAPI myNtSetValueKey(HANDLE KeyHandle, PUNICODE_STRING ValueName, ULONG TitleIndex, ULONG Type,
                                          PVOID Data, ULONG DataSize);

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static NTSTATUS NTAPI myNtQueryValueKey(HANDLE KeyHandle, PUNICODE_STRING ValueName,
                                            KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
                                            PVOID KeyValueInformation,
                                            ULONG Length, PULONG ResultLength);

    /**
        * function that will replace original one. for parameters and more documentation check MSDN
        */
    static NTSTATUS
    NTAPI myNtQueryKey(HANDLE KeyHandle, KEY_INFORMATION_CLASS KeyInformationClass, PVOID KeyInformation,
                       ULONG Length, PULONG ResultLength);

    /**
        * function that will replace original one. for parameters and more documentation check MSDN
        */
    static NTSTATUS NTAPI myNtRegCloseKey(HANDLE handle);

    /**
        * function that will replace original one. for parameters and more documentation check MSDN
        */
    static NTSTATUS
    NTAPI myNtOpenFile(PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                       PIO_STATUS_BLOCK IoStatusBlock, ULONG ShareAccess, ULONG OpenOptions);

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static NTSTATUS NTAPI myNtCreateFile
            (PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
             PIO_STATUS_BLOCK IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes,
             ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength);

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static NTSTATUS
    NTAPI myNtQueryAttributesFile(POBJECT_ATTRIBUTES ObjectAttributes, PFILE_BASIC_INFORMATION FileInformation);

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static BOOL WINAPI myMoveFileW(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName);;

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static BOOL WINAPI myMoveFileExW(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName, DWORD dwFlags);;

//**NON REGISTER FUNCTIONS**//

    /**
        * function that will replace original one. for parameters and more documentation check MSDN
        */
    static BOOL WINAPI
    myCreateProcessW(LPCWSTR lpApplicationName, LPWSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes,
                     LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags,
                     LPVOID lpEnvironment, LPCWSTR lpCurrentDirectory, LPSTARTUPINFOW lpStartupInfo,
                     LPPROCESS_INFORMATION lpProcessInformation);

    /**
    * function that will replace original one. for parameters and more documentation check MSDN
    */
    static HMODULE WINAPI myLoadLibraryW(_In_ LPCWSTR lpLibFileName);

//**STEALING FUNCTIONS**//

    /**
     * Swaps all functions that are needed for sandbox to function
     * @param pimageDosHeader pointer to ImageDosHeader
     * @param action stral or unsteal functions
     */
    static void WINAPI locateAndSwapAll(PIMAGE_DOS_HEADER pimageDosHeader, ACTION action);

    /**
     * performs swap of 2 function addresses. imported by ordinal or by name. multiple calls to this function with the
     * same parameters will not resolve in the loop.
     * @tparam T
     * @tparam F
     * @param fName
     * @param pImageImpDescArray
     * @param pDosHeader
     * @param pNTHeaders
     * @param func1 function 1
     * @param imageDescIndex
     * @param thunkIndex
     * @param func2 function 2
     */
    template<typename T>
    static void
    WINAPI functionSwap(char *&fName, PIMAGE_IMPORT_DESCRIPTOR &pImageImpDescArray, PIMAGE_DOS_HEADER &pDosHeader,
                        PIMAGE_NT_HEADERS &pNTHeaders, MAX_WORD func1,
                        size_t &imageDescIndex, size_t &thunkIndex, T &func2);

    /**
     * Get function name imported by ordinal or by name
     * @param imageThunkData
     * @param pDosHeader
     * @param pNTHeaders
     * @return
     */
    static char *WINAPI
    getFunctionName(IMAGE_THUNK_DATA &imageThunkData, PIMAGE_DOS_HEADER &pDosHeader, PIMAGE_NT_HEADERS &pNTHeaders);

    /**
     * locates function by their names and swaps them
     * @tparam T
     * @tparam F
     * @param pDosHeader
     * @param dllName
     * @param replaceFunkName
     * @param func1 function 1 (when hooking function from sandbox dll)
     * @param func2 function 2 (when hooking function from original dll)
     * @param action
     */
    template<typename T>
    static void
    WINAPI locateFunctionAndSwap(PIMAGE_DOS_HEADER pDosHeader, const char *dllName, const char *replaceFunkName,
                                 T &func1, T &func2, ACTION action);

    /**
     * initialize sandbox
     */
    static void WINAPI init();

    /**
     * finish sandbox
     */
    static void WINAPI done();

    /**
     * load sandbox dll into dynamically created process
     * @param pid  pid
     */
    static void WINAPI loadSandboxByPID(size_t pid);
};


