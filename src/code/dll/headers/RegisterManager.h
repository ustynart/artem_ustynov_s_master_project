//
// Created by Artem Ustynov on 22.03.2022.
//

#pragma once

#include <map>
#include <string>
#include <windows.h>
#include <ostream>
#include <vector>
#include <winternl.h>
#include "sqlite3.h"
#include "Defines.h"
#include <ntstatus.h>
#include "OriginalWindowsDefinitions.h"

using namespace std;
/** Multithreading is handled in hijacker.cpp" **/

class RegisterManager {
    /**
     * stores information about registry value
     */
    class MyRegValue {
        HANDLE KeyHandle = NULL;

    public:
        PUNICODE_STRING ValueName = NULL;
        ULONG TitleIndex = NULL;
        ULONG Type = NULL;
        PVOID Data = NULL;
        ULONG DataSize = NULL;
        NTSTATUS status = STATUS_SUCCESS;

        MyRegValue(HANDLE &keyHandle, PUNICODE_STRING &valueName, ULONG titleIndex, ULONG type, PVOID data,
                   ULONG dataSize);

        MyRegValue() {}
    };

    /**
    * stores information open registry
    */
    class MyRegKeyOpen {
        ACCESS_MASK accessMask;
        ULONG OpenOptions;
    public:
        HANDLE resHandle = NULL;
        int reference = 0;
        bool emulated = false;
        map<wstring, MyRegValue> created_values;
        OBJECT_ATTRIBUTES objectAttributes;

        MyRegKeyOpen(ACCESS_MASK accessMask, POBJECT_ATTRIBUTES objectAttributes, ULONG openOptions,
                     PHANDLE PKeyHandle);

        explicit MyRegKeyOpen(PHANDLE PKeyHandle);

        MyRegKeyOpen() {}
    };

    sqlite3 *db{};

    /**
     * locate open registry by it's name
     */
    map<wstring, MyRegKeyOpen> openedRegisters;
    /**
     * locate open registry by it's handle
     */
    map<MAX_WORD, wstring> openedRegisterLocator;

    /**
     * Copies unicode string
     * @param dst destination unicode string
     * @param src source unicode string
     */
    static void copyUnicodeString(PUNICODE_STRING &dst, PUNICODE_STRING &src);;

    /**
   * Copies wstring
   * @param dst destination wstring
   * @param src source wstring
   */
    static void copyName(LPWSTR &dst, LPCWSTR src);;

    /**
     * copies data
     * @param dst destination allocated memory
     * @param src source
     * @param size size of data to be copied (in bytes)
     */
    static void copyData(PVOID &dst, CONST BYTE *src, size_t size);
    /**
     * copy handle from PHANDLE
     * @tparam T
     * @tparam F
     * @param dst destination value
     * @param src source value
     */
    template<typename T, typename F>
    static void copyFromPtr(F &dst, T &src);

    /**
     * gets key path in the full form (\REGISTRY\MACHINE\...)
     * @param handle handle
     * @return wstring key path
     */
    wstring getKeyPathFromHandle(HANDLE handle);
    /**
     * writes value into sqlite DB
     * @param keyName keyname
     * @param valueName value name
     * @param type type of data
     * @param data data itself
     * @param length length in bytes
     * @param action type of action that will be performed (create_value, create_key, query_value)
     */
    void createOrUpdateValue(const WCHAR *keyName, const WCHAR *valueName, int type, PVOID data, ULONG length,
                             const WCHAR *action);

    static vector<PVOID> allocatedPointers;
public:
    virtual ~RegisterManager();

public:
    static bool SQL_OPERATION;

    RegisterManager();

    //**REGISTRY FUNCTIONS**//
    /**
   * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
   */
    void
    ntCreateKey(PHANDLE &KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, ULONG TitleIndex,
                PUNICODE_STRING Class, ULONG CreateOptions, PULONG Disposition);
    /**
   * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
   */
    NTSTATUS
    ntOpenKey(PHANDLE &KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES &ObjectAttributes, ULONG OpenOptions,
              bool emulated = false);
    /**
   * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
   */
    NTSTATUS ntSetValue(HANDLE &KeyHandle, PUNICODE_STRING &ValueName, ULONG TitleIndex, ULONG Type,
                        PVOID Data, ULONG DataSize);
    /**
   * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
   */
    NTSTATUS NTAPI ntQueryValueKey(HANDLE &KeyHandle, PUNICODE_STRING &ValueName,
                                   KEY_VALUE_INFORMATION_CLASS &KeyValueInformationClass, PVOID &KeyValueInformation,
                                   ULONG Length, PULONG &ResultLength);
    /**
   * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
   */
    NTSTATUS NTAPI ntQueryKey(HANDLE &KeyHandle, KEY_INFORMATION_CLASS &KeyInformationClass, PVOID &KeyInformation,
                              ULONG &Length, PULONG &ResultLength);
    /**
   * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
   */
    NTSTATUS NTAPI ntCloseKey(HANDLE &handle);

    /**
     * loads information about queried register
     * @param informationClass windows information class
     * @param valMap map that matches key name to opened register
     * @param valName value name
     * @param ValueName  value name as a unicode string
     * @param KeyHandle handle to the key
     * @param KeyValueInformation pointer to KeyValueInformation
     */
    static void loadInfoIntoReg(KEY_VALUE_INFORMATION_CLASS informationClass, map<wstring, MyRegValue> &valMap,
                                wstring &valName, PUNICODE_STRING &ValueName, HANDLE &KeyHandle,
                                PVOID &KeyValueInformation);
    /**
     * print content of maps. for debug purposes
     */
    void print();
    /**
     * create sqlite table if one doesn't exist already
     */
    void createTableIfNotExits();
    /**
     * free allocated memory
     */
    static void freeMemory();
};



