//
// Created by Artem Ustynov on 23.03.2022.
//

#pragma once

#include <regex>
#include <windows.h>
#include <map>
#include <winternl.h>
#include "OriginalWindowsDefinitions.h"
#include <ntstatus.h>
#include <filesystem>
/**
 *  RELATIVE PATH IS NOT SUPPORTED
 *
 * Multithreading is handled in hijacker.cpp"
 * **/

using namespace std;
/**
 * Manages file related operations. Including dll and executables redirection to the sandbox
 *
 */
class FileManager {
    /**
     * stores information mapping original file name to it's counterpart in the sandbox
     */
    map<wstring, wstring> translation_map;
    /**
     * if the call must invoke original function, this variable should be set.
     */
    static bool originalCall;

    /**
     * special characters that need to be taken care os
     */
    const wregex specialChars{LR"([-[\]{}()*+?.,\^$|#\s\\:])"};

    /**
     * generates path inside the sandbox
     * @param originalPath wstring original path
     * @return wstring new path inside the sandbox
     */
    wstring *generateNewPath(wstring &originalPath);

    /**
     * Generates new unicode string
     * @param newPath path that unicode string should contain
     * @return pointer to new unicode string
     */
    static PUNICODE_STRING generateNewUnicode(wstring *newPath);

    /**
     * holds all created pointers, so they can be removed in the end
     */
    static vector<wstring *> allocatedWstrings;
    static vector<UNICODE_STRING *> allocatedUstrings;
public:
    virtual ~FileManager();
    /**
     * path to the sandbox folder
     */
    static wstring swampPath;
    /**
     * if dll library needs to be loaded and it is not managed by sandbox, this should set to true.
     * not copying dll libraries to improve speed.
     */
    static bool libOriginalCall;
    /**
     * if sqloperation is set original functions should be called
     */
    static bool SQL_OPERATION;

    /**
     *release allocated memory
     */
    static void freeMemory();

    /**
     * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
     */
    NTSTATUS NTAPI ntOpenFile(PHANDLE &FileHandle, ACCESS_MASK &DesiredAccess, POBJECT_ATTRIBUTES &ObjectAttributes,
                              PIO_STATUS_BLOCK &IoStatusBlock, ULONG &ShareAccess, ULONG &OpenOptions);
    /**
     * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
     */
    NTSTATUS NTAPI ntCreateFile // create is used as open
            (PHANDLE &FileHandle, ACCESS_MASK &DesiredAccess, POBJECT_ATTRIBUTES &ObjectAttributes,
             PIO_STATUS_BLOCK &IoStatusBlock, PLARGE_INTEGER &AllocationSize, ULONG &FileAttributes,
             ULONG &ShareAccess, ULONG &CreateDisposition, ULONG &CreateOptions, PVOID &EaBuffer, ULONG &EaLength);
    /**
     * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
     */
    NTSTATUS
    NTAPI ntQueryAttributesFile(POBJECT_ATTRIBUTES &ObjectAttributes, PFILE_BASIC_INFORMATION &FileInformation);
    /**
     * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
     */
    BOOL WINAPI moveFileW(LPCWSTR &lpExistingFileName, LPCWSTR &lpNewFileName);
    /**
     * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
     */
    BOOL WINAPI moveFileExW(LPCWSTR &lpExistingFileName, LPCWSTR &lpNewFileName, DWORD Flags);

    /**
     * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
     */
    HMODULE WINAPI loadLibraryW(LPCWSTR &lpLibFileName);

    /**
     * mimics windows function, making sure all operations are contained within a sandbox. see MSDN for parameters
     */
    BOOL
    WINAPI createProcessW(LPCWSTR &lpApplicationName, LPWSTR &lpCommandLine, LPSECURITY_ATTRIBUTES &lpProcessAttributes,
                          LPSECURITY_ATTRIBUTES &lpThreadAttributes, BOOL &bInheritHandles, DWORD dwCreationFlags,
                          LPVOID &lpEnvironment, LPCWSTR &lpCurrentDirectory, LPSTARTUPINFOW &lpStartupInfo,
                          LPPROCESS_INFORMATION &lpProcessInformation);
};


