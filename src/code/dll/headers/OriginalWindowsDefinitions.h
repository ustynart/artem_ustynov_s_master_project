//
// Created by Artem Ustynov on 22.03.2022.
//

#pragma once

#include <windows.h>
#include <winternl.h>

/**
 * stores windows structures and functions definitions for convenient use
 */


/*** STRUCTURES ***/

typedef enum _KEY_VALUE_INFORMATION_CLASS {
    KeyValueBasicInformation,
    KeyValueFullInformation,
    KeyValuePartialInformation,
    KeyValueFullInformationAlign64,
    KeyValuePartialInformationAlign64,
    KeyValueLayerInformation,
    MaxKeyValueInfoClass
} KEY_VALUE_INFORMATION_CLASS;

typedef struct _KEY_VALUE_PARTIAL_INFORMATION {
    ULONG TitleIndex;
    ULONG Type;
    ULONG DataLength;
    UCHAR Data[1];
} KEY_VALUE_PARTIAL_INFORMATION, *PKEY_VALUE_PARTIAL_INFORMATION;

typedef struct _KEY_VALUE_FULL_INFORMATION {
    ULONG TitleIndex;
    ULONG Type;
    ULONG DataOffset;
    ULONG DataLength;
    ULONG NameLength;
    WCHAR Name[1];
} KEY_VALUE_FULL_INFORMATION, *PKEY_VALUE_FULL_INFORMATION;

typedef struct _KEY_NAME_INFORMATION {
    ULONG NameLength;
    WCHAR *Name;
} KEY_NAME_INFORMATION, *PKEY_NAME_INFORMATION;

typedef enum _KEY_INFORMATION_CLASS {
    KeyBasicInformation,
    KeyNodeInformation,
    KeyFullInformation,
    KeyNameInformation,
    KeyCachedInformation,
    KeyFlagsInformation,
    KeyVirtualizationInformation,
    KeyHandleTagsInformation,
    KeyTrustInformation,
    KeyLayerInformation,
    MaxKeyInfoClass
} KEY_INFORMATION_CLASS;

typedef struct _FILE_BASIC_INFORMATION {
    LARGE_INTEGER CreationTime;
    LARGE_INTEGER LastAccessTime;
    LARGE_INTEGER LastWriteTime;
    LARGE_INTEGER ChangeTime;
    ULONG FileAttributes;
} FILE_BASIC_INFORMATION, *PFILE_BASIC_INFORMATION;

/*** FUNCTIONS ***/
class OriginalWindowsDefinitions {
public:
    static NTSTATUS
    (NTAPI *originalNtCreateKey)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG, PUNICODE_STRING, ULONG, PULONG);

    static NTSTATUS (NTAPI *originalNtOpenKeyEx)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG);

    static NTSTATUS (NTAPI *originalNtSetValueKey)(HANDLE, PUNICODE_STRING, ULONG, ULONG, PVOID, ULONG);

    static NTSTATUS (NTAPI *originalNtQueryValueKey)
            (HANDLE, PUNICODE_STRING, KEY_VALUE_INFORMATION_CLASS, PVOID, ULONG, PULONG);

    static NTSTATUS (NTAPI *originalNtQueryKey)(HANDLE, KEY_INFORMATION_CLASS, PVOID, ULONG, PULONG);

    static NTSTATUS (NTAPI *originalNtCloseKey)(HANDLE);

    static NTSTATUS
    (NTAPI *originalNtOpenFile)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK, ULONG, ULONG);

    static NTSTATUS
    (NTAPI *originalNtCreateFile)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK, PLARGE_INTEGER, ULONG,
                                  ULONG, ULONG, ULONG, PVOID, ULONG);

    static NTSTATUS (NTAPI *originalNtQueryAttributesFile)(POBJECT_ATTRIBUTES, PFILE_BASIC_INFORMATION);

    static BOOL (WINAPI *originalMoveFileW)(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName);

    static BOOL (WINAPI *originalMoveFileExw)(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName, DWORD dwFlags);

//**NON REGISTER FUNCTIONS**//
    static HMODULE (WINAPI *originalLoadLibraryW)(LPCWSTR lpLibFileName);

    static BOOL
    (WINAPI *originalCreateProcessW)(LPCWSTR, LPWSTR, LPSECURITY_ATTRIBUTES, LPSECURITY_ATTRIBUTES, BOOL, DWORD, LPVOID,
                                     LPCWSTR, LPSTARTUPINFOW, LPPROCESS_INFORMATION);
};