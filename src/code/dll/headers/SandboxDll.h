//
// Created by Artem Ustynov on 22.03.2022.
//

#pragma once

#include <windows.h>
#include "Hijacker.h"

/**
 * Starting point of function hijacking. see MSDN for parameters
 */
static BOOL WINAPI
DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved);
