//
// Created by Artem Ustynov on 22.03.2022.
//

#include "headers/RegisterManager.h"

bool RegisterManager::SQL_OPERATION = false;
vector<PVOID> RegisterManager::allocatedPointers;

void RegisterManager::copyUnicodeString(PUNICODE_STRING &dst, PUNICODE_STRING &src) {
    if (src == nullptr) return;
    if (src == dst) return;
    dst = (PUNICODE_STRING) malloc(sizeof(UNICODE_STRING));
    if (!dst)
        exit(3);
    allocatedPointers.push_back(dst);
    dst->Length = src->Length;
    dst->MaximumLength = src->MaximumLength;
    copyName(dst->Buffer, src->Buffer);
}

void RegisterManager::copyName(LPWSTR &dst, LPCWSTR src) {
    if (src != nullptr && src == dst) exit(10);
    if (src != nullptr) {
        auto len = wcslen(src) + 1;
        auto size = (len) * sizeof(WCHAR);
        dst = (PWCHAR) malloc(size);
        if (!dst)
            exit(3);
        allocatedPointers.push_back(dst);
        wcsncpy_s(dst, len, src, len - 1);

    } else {
        dst = (PWCHAR) malloc(1 * sizeof(WCHAR));
        if (!dst)
            exit(3);
        allocatedPointers.push_back(dst);
        dst[0] = L'\0';
    }
}

void RegisterManager::copyData(PVOID &dst, const BYTE *src, size_t size) {
    if (src != nullptr && src != dst) {
        dst = (PVOID) malloc(size);
        if (!dst)
            exit(3);
        allocatedPointers.push_back(dst);
        memcpy(dst, src, size);
    }
}

template<typename T, typename F>
void RegisterManager::copyFromPtr(F &dst, T &src) { if (src != NULL) dst = *src; }

wstring RegisterManager::getKeyPathFromHandle(HANDLE handle) {
    wstring keyPath;

    if (handle != NULL) {
        DWORD size = 0;
        DWORD result = 0;
        result = OriginalWindowsDefinitions::originalNtQueryKey(handle, KeyNameInformation, 0, 0, &size);
        if (result == STATUS_BUFFER_TOO_SMALL) {
            size = size + 2;
            auto buffer = (WCHAR *) malloc(size * sizeof(WCHAR)); // size is in bytes
            if (!buffer)
                exit(3);
            result = OriginalWindowsDefinitions
            ::originalNtQueryKey(handle, KeyNameInformation, buffer, size, &size);
            if (result == STATUS_SUCCESS) {
                buffer[size / sizeof(WCHAR)] = L'\0';
                keyPath = wstring(buffer + 2);
            }
            free(buffer);
        }
    }

    return keyPath;
}

void RegisterManager::createOrUpdateValue(const WCHAR *keyName, const WCHAR *valueName, int type, PVOID data,
                                          ULONG length, const WCHAR *action) {
    SQL_OPERATION = true;
    const auto sql_command = u"INSERT OR REPLACE INTO register_values"
                             "(key_name, name, type, data, action)"
                             "VALUES (?, ?, ?, ?, ?)";

    sqlite3_stmt *statement;
    auto rc = sqlite3_prepare16(db, sql_command, -1, &statement, NULL);
    if (rc != SQLITE_OK) exit(5);

    fflush(stdout);
    rc = sqlite3_bind_text16(statement, 1, keyName, (wcslen(keyName) + 1) * sizeof(WCHAR), SQLITE_TRANSIENT);
    if (rc != SQLITE_OK) exit(5);

    fflush(stdout);
    rc = sqlite3_bind_text16(statement, 2, valueName, (wcslen(valueName) + 1) * sizeof(WCHAR),
                             SQLITE_TRANSIENT);
    fflush(stdout);
    if (rc != SQLITE_OK) exit(5);

    rc = sqlite3_bind_int(statement, 3, (int) type);
    fflush(stdout);
    if (rc != SQLITE_OK) exit(5);

    rc = sqlite3_bind_blob(statement, 4, data, length, SQLITE_TRANSIENT);
    fflush(stdout);
    if (rc != SQLITE_OK) exit(5);

    rc = sqlite3_bind_text16(statement, 5, action, (wcslen(action) + 1) * sizeof(WCHAR), SQLITE_TRANSIENT);
    if (rc != SQLITE_OK) exit(5);

    rc = sqlite3_step(statement);

    if (rc != SQLITE_DONE) exit(5);

    rc = sqlite3_finalize(statement);
    if (rc != SQLITE_OK) exit(5);
    SQL_OPERATION = false;
}

void RegisterManager::createTableIfNotExits() {
    SQL_OPERATION = true;
    const auto sql_command = u"CREATE TABLE IF NOT EXISTS register_values ("
                             "id	INTEGER NOT NULL UNIQUE,"
                             "key_name	TEXT NOT NULL COLLATE NOCASE,"
                             "name	TEXT COLLATE NOCASE,"
                             "type	INTEGER NOT NULL,"
                             "data	BLOB NOT NULL,"
                             "action	TEXT NOT NULL,"
                             "PRIMARY KEY(id AUTOINCREMENT));";

    sqlite3_stmt *statement;
    auto rc = sqlite3_prepare16(db, sql_command, -1, &statement, NULL);
    if (rc != SQLITE_OK) exit(5);

    rc = sqlite3_step(statement);

    if (rc != SQLITE_DONE) exit(5);

    rc = sqlite3_finalize(statement);
    if (rc != SQLITE_OK) exit(5);
    SQL_OPERATION = false;
}

RegisterManager::RegisterManager() {
    auto rc = sqlite3_open("./registers.db", &db);
    if (rc) {
        exit(1);
    }
    createTableIfNotExits();
}

wstring generateKeyName(PHANDLE &KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                        ULONG CreateOptions) {
    return wstring(to_wstring((MAX_WORD) ObjectAttributes->RootDirectory) + L"\\" +
                   ObjectAttributes->ObjectName->Buffer);
}

void RegisterManager::ntCreateKey(PHANDLE &KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes,
                                  ULONG TitleIndex, PUNICODE_STRING Class, ULONG CreateOptions, PULONG Disposition) {

    auto key_name = generateKeyName(KeyHandle, DesiredAccess, ObjectAttributes, CreateOptions);



    if (openedRegisters.find(key_name) != end(openedRegisters)) {
        ntOpenKey(KeyHandle, DesiredAccess, ObjectAttributes, 0, false);
        *Disposition = REG_OPENED_EXISTING_KEY;
        return;
    } else {
        ntOpenKey(KeyHandle, DesiredAccess, ObjectAttributes, 0, true);
        *Disposition = REG_CREATED_NEW_KEY;
    }
}

NTSTATUS RegisterManager::ntOpenKey(PHANDLE &KeyHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES &ObjectAttributes,
                                    ULONG OpenOptions, bool emulated) {

    auto key_name = generateKeyName(KeyHandle, DesiredAccess, ObjectAttributes, OpenOptions);
//    E:\Downloads\innosetup-6.2.0.exe
    auto res = STATUS_SUCCESS;
    if (openedRegisters.find(key_name) != end(openedRegisters)) {
        *KeyHandle =openedRegisters.at(key_name).resHandle;
        openedRegisters.at(key_name).reference += 1;
        return res;
    }

    res = OriginalWindowsDefinitions::originalNtOpenKeyEx(KeyHandle, DesiredAccess, ObjectAttributes, OpenOptions);
    if (emulated) {
        openedRegisters[key_name] = MyRegKeyOpen(DesiredAccess, ObjectAttributes, OpenOptions, KeyHandle);
        openedRegisters.at(key_name).emulated = true;
        *KeyHandle = (HANDLE) &(openedRegisters.at(key_name).created_values);
        openedRegisters.at(key_name).resHandle =  *KeyHandle;
        openedRegisterLocator[(MAX_WORD) *KeyHandle] = key_name;

        createOrUpdateValue(ObjectAttributes->ObjectName->Buffer, L" ", 0, ObjectAttributes->ObjectName, 0,
                            L"create_key");

        return STATUS_SUCCESS;
    }
    if (res == STATUS_SUCCESS) {
        openedRegisters[key_name] = MyRegKeyOpen(DesiredAccess, ObjectAttributes, OpenOptions, KeyHandle);
        /**   return value can be used as root directory of another open key call, thus returning original handle,
           on nt level will intercept all write calls
          *KeyHandle = (HKEY) &(opened_registers[key_name].created_values);**/
        openedRegisterLocator[(MAX_WORD) *KeyHandle] = key_name;
    }

    return res;
}

NTSTATUS
RegisterManager::ntSetValue(HANDLE &KeyHandle, PUNICODE_STRING &ValueName, ULONG TitleIndex, ULONG Type, PVOID Data,
                            ULONG DataSize) {
    if (!KeyHandle) return STATUS_INVALID_HANDLE;
    auto value_name = ValueName->Buffer ? wstring(ValueName->Buffer) : L"";

    if (openedRegisterLocator.find((MAX_WORD) KeyHandle) == end(openedRegisterLocator)) {
        auto full_key_name = getKeyPathFromHandle(KeyHandle);
        openedRegisters[full_key_name] = MyRegKeyOpen(&KeyHandle);
        openedRegisterLocator[(MAX_WORD) KeyHandle] = full_key_name;
        createOrUpdateValue(full_key_name.data(), value_name.data(), Type, Data, DataSize, L"create_value");
    }
    auto key_name = openedRegisterLocator.at((MAX_WORD) KeyHandle);
    openedRegisters.at(key_name).created_values[value_name] =
            MyRegValue(KeyHandle, ValueName, TitleIndex, Type, Data, DataSize);
    createOrUpdateValue(key_name.data(), value_name.data(), Type, Data, DataSize, L"create_value");
    return STATUS_SUCCESS;
}

NTSTATUS RegisterManager::ntQueryValueKey(HANDLE &KeyHandle, PUNICODE_STRING &ValueName,
                                          KEY_VALUE_INFORMATION_CLASS &KeyValueInformationClass,
                                          PVOID &KeyValueInformation, ULONG Length, PULONG &ResultLength) {

    auto res = STATUS_SUCCESS;
    auto full_key_name = getKeyPathFromHandle(KeyHandle);
    auto information = (PKEY_VALUE_PARTIAL_INFORMATION) KeyValueInformation;
    auto val_name = ValueName->Buffer ? wstring(ValueName->Buffer) : L"";
    //work with handles that were not opened within a program
    if (openedRegisterLocator.find((MAX_WORD) KeyHandle) == end(openedRegisterLocator)) {
        auto res = OriginalWindowsDefinitions::originalNtQueryValueKey(KeyHandle, ValueName,
                                                                       KeyValueInformationClass,
                                                                       KeyValueInformation,
                                                                       Length, ResultLength);
        if (res == STATUS_SUCCESS)
            createOrUpdateValue(full_key_name.data(), val_name.data(), information->Type, information->Data,
                                information->DataLength, L"query_value");
        return res;
    }
    auto key_name = openedRegisterLocator.at((MAX_WORD) KeyHandle);

    if (openedRegisters.find(key_name) != end(openedRegisters)) {
        auto &val_map = openedRegisters.at(key_name).created_values;


        if (val_map.find(val_name) != end(val_map)) {
            if (val_map.find(val_name) != end(val_map) && val_map.at(val_name).Data != nullptr) {
                auto reg_value = val_map.at(val_name);
                *ResultLength = reg_value.DataSize + sizeof(PKEY_VALUE_PARTIAL_INFORMATION);
                if (Length < *ResultLength) {
                    return STATUS_BUFFER_TOO_SMALL;
                }
                information->TitleIndex = reg_value.TitleIndex;
                information->Type = reg_value.Type;
                information->DataLength = reg_value.DataSize;
                memcpy(information->Data, reg_value.Data, reg_value.DataSize);

            }
        } else {
            auto &realHandle = openedRegisters.at(key_name).resHandle;
            res = OriginalWindowsDefinitions::originalNtQueryValueKey(realHandle, ValueName,
                                                                      KeyValueInformationClass, KeyValueInformation,
                                                                      Length, ResultLength);
            if (res != STATUS_SUCCESS) return res;
            loadInfoIntoReg(KeyValueInformationClass, val_map, val_name, ValueName, realHandle,
                            KeyValueInformation);

        }
    } else return ERROR_FILE_NOT_FOUND;
    createOrUpdateValue(full_key_name.data(), val_name.data(), information->Type, information->Data,
                        information->DataLength, L"query_value");
    return res;
}

NTSTATUS
RegisterManager::ntQueryKey(HANDLE &KeyHandle, KEY_INFORMATION_CLASS &KeyInformationClass, PVOID &KeyInformation,
                            ULONG &Length, PULONG &ResultLength) {
    if (!KeyHandle) return STATUS_INVALID_HANDLE;
    if (openedRegisterLocator.find(((DWORD) KeyHandle) - 2) == end(openedRegisterLocator)) {
        return OriginalWindowsDefinitions::originalNtQueryKey(KeyHandle, KeyInformationClass, KeyInformation, Length,
                                                              ResultLength);
    }
    auto key_name = openedRegisterLocator.at(((DWORD) KeyHandle) - 2);
    auto key_register = openedRegisters.at(key_name);
    if (!openedRegisters.at(key_name).emulated) {
        return OriginalWindowsDefinitions::originalNtQueryKey(KeyHandle, KeyInformationClass, KeyInformation, Length,
                                                              ResultLength);
    }
    auto &information = *(PKEY_NAME_INFORMATION) KeyInformation;
    information.NameLength = key_register.objectAttributes.ObjectName->Length;
    wcsncpy(information.Name, key_register.objectAttributes.ObjectName->Buffer, information.NameLength);
    return STATUS_SUCCESS;
}

NTSTATUS RegisterManager::ntCloseKey(HANDLE &handle) {
//
    if (!handle) return STATUS_SUCCESS; // maybe success
    if (openedRegisterLocator.find((MAX_WORD) handle) == end(openedRegisterLocator)) {
        return OriginalWindowsDefinitions::originalNtCloseKey(handle);
    }
    auto key_name = openedRegisterLocator.at((MAX_WORD) handle);
    auto m_handle = openedRegisters.at(key_name).resHandle;
    if (openedRegisters.at(key_name).emulated) {
        openedRegisters.at(key_name).reference -= 1;
        return STATUS_SUCCESS;
    }
    auto res = OriginalWindowsDefinitions::originalNtCloseKey(m_handle);
    openedRegisters.at(key_name).reference -= 1;
    if (openedRegisters.at(key_name).reference == 0) {
        openedRegisters.erase(key_name);
        openedRegisterLocator.erase((MAX_WORD) handle);
    }
    return res;
}

void RegisterManager::loadInfoIntoReg(KEY_VALUE_INFORMATION_CLASS informationClass, map<wstring, MyRegValue> &valMap,
                                      wstring &valName, PUNICODE_STRING &ValueName, HANDLE &KeyHandle,
                                      PVOID &KeyValueInformation) {
    switch (informationClass) {
        case KeyValueFullInformation: {
            auto &type = ((PKEY_VALUE_FULL_INFORMATION) KeyValueInformation)->Type;
            auto &dataOffset = ((PKEY_VALUE_FULL_INFORMATION) KeyValueInformation)->DataOffset;
            auto &dataLength = ((PKEY_VALUE_FULL_INFORMATION) KeyValueInformation)->DataLength;
            auto data = (MAX_WORD) (((PKEY_VALUE_FULL_INFORMATION) KeyValueInformation)->Name) + dataOffset;
            valMap[valName] = MyRegValue(KeyHandle, ValueName, NULL, type, (PVOID) data, dataLength);
            break;
        }
        case KeyValuePartialInformation: {
            auto &type = ((PKEY_VALUE_PARTIAL_INFORMATION) KeyValueInformation)->Type;
            auto &dataLength = ((PKEY_VALUE_PARTIAL_INFORMATION) KeyValueInformation)->DataLength;
            auto &data = (((PKEY_VALUE_PARTIAL_INFORMATION) KeyValueInformation)->Data);
            valMap[valName] = MyRegValue(KeyHandle, ValueName, NULL, type, data, dataLength);
            break;
        }
        default:
            throw exception("UNKNOWN INFORMATION EXITING IMMEDIATELY !!\n");
    }
}

void RegisterManager::print() {

    FILE *fPtr;
    fPtr = fopen(R"(.\my_swamp\log.txt)", "a+");

    fprintf(fPtr, "\n=====Opened KEYS=====\n");
    for (auto &[key, value]: openedRegisters) {
        fprintf(fPtr, "\tOpened KEy: %ls\n", key.data());
        for (auto &[val_key, val_value]: value.created_values) {
            fprintf(fPtr, "\tACCESSED VALUE: %ls\n", val_key.data());
        }
    }
    fclose(fPtr);
}

void RegisterManager::freeMemory() {
    for (auto &pointer: allocatedPointers) {
        free(pointer);
    }
    allocatedPointers.clear();
}

RegisterManager::~RegisterManager() {
    freeMemory();
}

RegisterManager::MyRegValue::MyRegValue(HANDLE &keyHandle, PUNICODE_STRING &valueName, ULONG titleIndex, ULONG type,
                                        PVOID data, ULONG dataSize) : KeyHandle(keyHandle) {
    this->TitleIndex = titleIndex;
    this->Type = type;
    this->DataSize = dataSize;

    copyUnicodeString(ValueName, valueName);
    copyData(Data, (PBYTE) data, dataSize);
}

RegisterManager::MyRegKeyOpen::MyRegKeyOpen(ACCESS_MASK accessMask, POBJECT_ATTRIBUTES objectAttributes,
                                            ULONG openOptions, PHANDLE PKeyHandle) :
        accessMask(accessMask), OpenOptions(openOptions) {
    reference = 1;
    copyFromPtr(this->resHandle, PKeyHandle);
    if (objectAttributes != NULL) {
        this->objectAttributes = *objectAttributes;
        copyUnicodeString(this->objectAttributes.ObjectName, objectAttributes->ObjectName);
    }
}

RegisterManager::MyRegKeyOpen::MyRegKeyOpen(PHANDLE PKeyHandle) {
    copyFromPtr(this->resHandle, PKeyHandle);
    reference = 1;
}
