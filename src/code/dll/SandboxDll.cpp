//
// Created by Artem Ustynov on 22.03.2022.
//

#include "headers/SandboxDll.h"

BOOL DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved) {
    switch (fdwReason) {
        case DLL_PROCESS_ATTACH:
            Hijacker::initCriticalSections();
            Hijacker::init();
            break;

        case DLL_PROCESS_DETACH:
            Hijacker::done();
            break;
    }
    return TRUE;  // Successful DLL_PROCESS_ATTACH.
}
