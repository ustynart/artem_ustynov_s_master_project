//
// Created by Artem Ustynov on 22.03.2022.
//

#include "headers/OriginalWindowsDefinitions.h"

NTSTATUS
(NTAPI *OriginalWindowsDefinitions::originalNtCreateKey)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG, PUNICODE_STRING, ULONG, PULONG)=nullptr;

NTSTATUS (NTAPI *OriginalWindowsDefinitions::originalNtOpenKeyEx)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG)=nullptr;

NTSTATUS (NTAPI *OriginalWindowsDefinitions::originalNtSetValueKey)(HANDLE, PUNICODE_STRING, ULONG, ULONG, PVOID, ULONG)=nullptr;

NTSTATUS (NTAPI *OriginalWindowsDefinitions::originalNtQueryValueKey)
        (HANDLE, PUNICODE_STRING, KEY_VALUE_INFORMATION_CLASS, PVOID, ULONG, PULONG)=nullptr;

NTSTATUS (NTAPI *OriginalWindowsDefinitions::originalNtQueryKey)(HANDLE, KEY_INFORMATION_CLASS, PVOID, ULONG, PULONG)=nullptr;

NTSTATUS (NTAPI *OriginalWindowsDefinitions::originalNtCloseKey)(HANDLE)=nullptr;

NTSTATUS (NTAPI *OriginalWindowsDefinitions::originalNtOpenFile)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK, ULONG, ULONG)=nullptr;

NTSTATUS
(NTAPI *OriginalWindowsDefinitions::originalNtCreateFile)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK, PLARGE_INTEGER, ULONG,
                                                          ULONG, ULONG, ULONG, PVOID, ULONG)=nullptr;

NTSTATUS (NTAPI *OriginalWindowsDefinitions::originalNtQueryAttributesFile)(POBJECT_ATTRIBUTES, PFILE_BASIC_INFORMATION)=nullptr;

BOOL (WINAPI *OriginalWindowsDefinitions::originalMoveFileW)(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName)=nullptr;

BOOL (WINAPI *OriginalWindowsDefinitions::originalMoveFileExw)(LPCWSTR lpExistingFileName, LPCWSTR lpNewFileName, DWORD dwFlags)=nullptr;

//**NON REGISTER FUNCTIONS**//
HMODULE (WINAPI *OriginalWindowsDefinitions::originalLoadLibraryW)(LPCWSTR lpLibFileName)=nullptr;

BOOL
(WINAPI *OriginalWindowsDefinitions::originalCreateProcessW)(LPCWSTR, LPWSTR, LPSECURITY_ATTRIBUTES, LPSECURITY_ATTRIBUTES, BOOL, DWORD, LPVOID,
                                                             LPCWSTR, LPSTARTUPINFOW, LPPROCESS_INFORMATION)=nullptr;
