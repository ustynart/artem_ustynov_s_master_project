//
// Created by Artem Ustynov on 23.03.2022.
//

#include "headers/Injector.h"

int main(int argc, char **argv) {
    Injector::main(argc, argv);
    return 0;
}