//
// Created by Artem Ustynov on 23.03.2022.
//

#include "headers/Injector.h"

int Injector::main(int argc, char **argv) {

    DWORD PID = stoi(argv[1]);

    HANDLE hProcess = OpenProcess(  PROCESS_VM_WRITE  | PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION, FALSE, PID);
    if (!hProcess) {
        exit(1);
    }
    BOOL bit_32;
    if (! IsWow64Process(hProcess, &bit_32))
        exit(6);
    string sStealDLLFileName = bit_32 ? R"(.\SandboxDll32.dll)" : R"(.\SandboxDll64.dll)";

    const char *cStealDLLFileName = sStealDLLFileName.c_str();
    DWORD dwStealDLLFileNameLen = strlen(cStealDLLFileName) + 1;
    void *lpDataAddr = VirtualAllocEx(hProcess, nullptr, dwStealDLLFileNameLen, MEM_RESERVE | MEM_COMMIT,
                                      PAGE_READWRITE);
    if (!lpDataAddr) {
        CloseHandle(hProcess);
        exit(1);
    }
    SIZE_T szBytesWritten;
    if (!WriteProcessMemory(hProcess, lpDataAddr, cStealDLLFileName, dwStealDLLFileNameLen, &szBytesWritten)) {
        CloseHandle(hProcess);
        exit(1);
    }
    HANDLE hThread = CreateRemoteThread(hProcess, nullptr, NULL, (LPTHREAD_START_ROUTINE) (&LoadLibraryA),
                                        lpDataAddr, NULL, nullptr);
    if (!hThread) {
        CloseHandle(hProcess);
        exit(1);
    }
    DWORD exit_code;
    WaitForSingleObject(hThread, INFINITE);
    GetExitCodeThread(hThread, &exit_code);
    CloseHandle(hThread);
    CloseHandle(hProcess);
    return exit_code;
}

