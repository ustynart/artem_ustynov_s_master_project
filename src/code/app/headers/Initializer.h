//
// Created by Artem Ustynov on 23.03.2022.
//

#pragma once

#include <stdio.h>
#include <Windows.h>
#include <memory>
#include <iostream>
#include <string>
#include <direct.h>

using namespace std;

class Initializer {
    /**
     * Starts process in the paused state
     * @param argc count of arguments for a process
     * @param argv new process
     * @param ptr_thread (out) process' thread handle
     * @return
     */
    static _PROCESS_INFORMATION startPausedProcess(int argc, wchar_t **argv, PHANDLE ptr_thread);

    static void printHelp();

public:
    static int wmain(int argc, wchar_t **argv);
};
