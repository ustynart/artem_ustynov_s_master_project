//
// Created by Artem Ustynov on 23.03.2022.
//

#include "headers/Initializer.h"


_PROCESS_INFORMATION
Initializer::startPausedProcess(int argc, wchar_t **argv, PHANDLE ptr_thread) // cleaned up a bit, but no RAII
{

    PROCESS_INFORMATION pi {};
    pi.dwProcessId = 0;
    if (ptr_thread == nullptr) return pi;
    STARTUPINFOW si{}; // initialize with zeroes.
    si.cb = sizeof(STARTUPINFOW);
    wstring params = L"";
    for (int i = 2; i < argc; i++) {
        params.append(wstring(L" ") + argv[i]);
    }
    if (!CreateProcessW(argv[1], params.data(), nullptr, nullptr, false, CREATE_SUSPENDED,
                        nullptr, nullptr, std::addressof(si), std::addressof(pi))) {
        *ptr_thread = nullptr;
        return pi;
    }

    *ptr_thread = pi.hThread;
    return pi;
}

void Initializer::printHelp() {
    printf("At least 1 argument expected: program path\n");
}

int Initializer::wmain(int argc, wchar_t **argv) {

    if (argc < 2) {
        printHelp();
        exit(1);
    }
    _mkdir("./my_swamp");
    HANDLE threadHandle = nullptr;
    auto proprocessInformationess = startPausedProcess(argc, argv, &threadHandle);
    if (proprocessInformationess.dwProcessId == 0) {
        printf("Process wasn't initialized, exitining");
        exit(1);
    }
    DWORD PID = proprocessInformationess.dwProcessId;
    HANDLE hProcess = OpenProcess(PROCESS_VM_WRITE | PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION, FALSE, PID);
    if (!hProcess) {
        printf("Failed to open the debugged app.\n");
        return 1;
    }
    BOOL bit_32;
    if (!IsWow64Process(hProcess, &bit_32)) {
        printf("can't figure out bitness of the process, exiting");
        if (!TerminateProcess(hProcess, 1)) {
            printf("can't close installer it is stuck in suspended state and has to be killed manually");
        }
        exit(1);
    }
    PROCESS_INFORMATION pi{};
    STARTUPINFOW si{}; // initialize with zeroes.
    si.cb = sizeof(STARTUPINFOW);
    auto args = wstring(L" ") + to_wstring(PID);
    if (bit_32) {
        wstring name = LR"(.\Injector32.exe)";
        if (CreateProcessW(name.data(), args.data(), NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi)) {
            printf("running 32 bit injector\n");
            DWORD exit_code = 0;
            WaitForSingleObject(pi.hProcess, INFINITE);
            GetExitCodeProcess(pi.hProcess, &exit_code);
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            if (exit_code!=ERROR_SUCCESS){
                printf("injection failed");
                exit(exit_code);
            }
        } else {
            printf("FAILED to run injector\n");
            exit(1);
        }
    } else {
        wstring name = LR"(.\Injector64.exe)";
        if (CreateProcessW(name.data(), args.data(), NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi)) {
            printf("running 64 bit injector\n");
            DWORD exit_code = 0;
            WaitForSingleObject(pi.hProcess, INFINITE);
            GetExitCodeProcess(pi.hProcess, &exit_code);
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            if (exit_code!=ERROR_SUCCESS){
                printf("injection failed");
                exit(exit_code);
            }
        } else {
            printf("FAILED to run injector\n");
            exit(1);
        }
    }
    DWORD exit_code = 0;
    if (proprocessInformationess.hProcess) {

        ResumeThread(threadHandle);

        WaitForSingleObject(threadHandle, INFINITE);
        GetExitCodeProcess(proprocessInformationess.hProcess, &exit_code);
        CloseHandle(threadHandle);
        CloseHandle(proprocessInformationess.hProcess);
    }
    CloseHandle(hProcess);
    printf("Finished.\n");
    return exit_code;
}