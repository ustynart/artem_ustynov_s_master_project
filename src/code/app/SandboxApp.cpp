//
// Created by Artem Ustynov on 23.03.2022.
//

#include "headers/Initializer.h"

int wmain(int argc, wchar_t *argv[])  {
    auto res = Initializer::wmain(argc, argv);
    return res;
}