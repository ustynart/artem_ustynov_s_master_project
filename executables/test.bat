ECHO OFF

echo test start
echo important data>C:\test232.txt
echo important data>C:\test264.txt
set STATUS=ok
@REM will start 64bit process that will run 32 bit process
start "test" /wait "Sandbox.exe" test.exe

if %ERRORLEVEL% equ 0 (
    echo test ok
) else (
    echo test fail. %ERRORLEVEL%
    set STATUS=not ok
)
 echo ===
  echo ===
@REM didn't work inside the loop. no time to fix.
reg query HKEY_CURRENT_USER\app32>nul 2>nul
    if %errorlevel% equ 0 (
        echo registry32 exists. FAIL
        set  STATUS=not ok
    ) else (
      echo registry32 doesn't exist. ok
    )

reg query HKEY_CURRENT_USER\app64>nul 2>nul
    if %errorlevel% equ 0 (
        echo registry64 exists. FAIL
        set  STATUS=not ok
    ) else (
      echo registry64 doesn't exist. ok
    )
 echo ===
 echo ===
for %%x in ( 32 , 64 ) do (

    IF EXIST C:\test%%x.txt (
        echo file%%x was created on disc. FAIL
        set  STATUS=not ok
    ) ELSE (
        echo file%%x not created outside sandbox. ok
    )

    set /p DATA=<C:\test2%%x.txt

    if "%DATA%"=="important data" (
        echo important%%x data same. ok
    ) else (
        echo important%%x data not same. %DATA% .FAIL
        set  STATUS="notok"
    )


    set /p TEST_FILE=<.\my_swamp\____C__test%%x_txt_test%%x.txt

    if "%TEST_FILE%"=="This is testing for fprintf..." (
        echo test%%x data ok
    ) else (
        echo test%%x data not ok. %TEST_FILE% .FAIL
        set STATUS="notok"
    )

    set /p TEST_FILE2=<.\my_swamp\____C__test2%%x_txt_test2%%x.txt

    if "%TEST_FILE2%"=="overriding sensetive data" (
        echo test2%%x data ok
    ) else (
        echo test%%x data not ok. %TEST_FILE2% .FAIL
        set  STATUS="notok"
    )
    echo ===
    echo ===
)

echo %STATUS%
if %STATUS%==ok (
    echo overall OK
    echo created files can be inspected at .\my_swamp and registries in registers.db
    del /f C:\test232.txt
    del /f C:\test264.txt
) else (
    echo overall FAIL. please see errors above
    echo will try to remove all of the possibly created files and registries
    del /f C:\test232.txt
    del /f C:\test32.txt
    reg delete "HKEY_CURRENT_USER\app32" /f

    del /f C:\test264.txt
    del /f C:\test64.txt
    reg delete "HKEY_CURRENT_USER\app64" /f
)

